<!--
SPDX-FileCopyrightText: 2021 David Hurka <david.hurka@mailbox.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Master Controller

This is a master controller which allows the Data Aggregator (DAE) to communicate with a string of power sensor modules.
The power sensor modules are interfaced via a 10MHz daisy-chained SPI bus.
The DAE is interfaced via full-speed USB.

The master controller is implemented on a 2-layer PCB with SMD components.
The PCB fits in a PCI or PCIe x1 low profile slot.

## Power supply

The controller is powered from the USB bus.

To provide robustness against careless test setups, the SPI bus is electrically isolated from the USB bus.
This means that the SPI bus needs an additional power supply.
This can be done by an on-board DC/DC converter or by additional power supply connections (5V or 9-24V).
In the latter case, it should be considered whether the power supply operates in standby mode.

## Auxiliary

The controller provides two relais on a 2.54mm IDC connector, which can be connected to the power and reset button of the SuT.
A power button is necessary on almost every automated test setup, so this is the only auxiliary function integrated in the master controller.
Additional auxiliary functionality can be connected to the GPIO header of any power sensor module.

The controller provides an own GPIO header, but this one is on the voltage level of the DAE, and therefore can be dangerous if used carelessly.
