EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB_B J?
U 1 1 6216F2D2
P 1750 2350
AR Path="/6216F2D2" Ref="J?"  Part="1" 
AR Path="/6216DC24/6216F2D2" Ref="J8"  Part="1" 
F 0 "J8" H 1807 2817 50  0000 C CNN
F 1 "USB_B" H 1807 2726 50  0000 C CNN
F 2 "Connector_USB:USB_B_OST_USB-B1HSxx_Horizontal" H 1900 2300 50  0001 C CNN
F 3 " ~" H 1900 2300 50  0001 C CNN
	1    1750 2350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 6216F2E6
P 1850 1350
AR Path="/6216F2E6" Ref="J?"  Part="1" 
AR Path="/6216DC24/6216F2E6" Ref="J7"  Part="1" 
F 0 "J7" H 1930 1342 50  0000 L CNN
F 1 "5VSB" H 1930 1251 50  0000 L CNN
F 2 "TerminalBlock_MetzConnect:TerminalBlock_MetzConnect_Type205_RT04502UBLC_1x02_P5.00mm_45Degree" H 1850 1350 50  0001 C CNN
F 3 "~" H 1850 1350 50  0001 C CNN
	1    1850 1350
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_TVS D?
U 1 1 6216F2F4
P 3900 2600
AR Path="/6216F2F4" Ref="D?"  Part="1" 
AR Path="/6216DC24/6216F2F4" Ref="D9"  Part="1" 
F 0 "D9" V 3854 2680 50  0000 L CNN
F 1 "P6SMB_5.0CA" V 3945 2680 50  0000 L CNN
F 2 "Diode_SMD:D_SMB_Handsoldering" H 3900 2600 50  0001 C CNN
F 3 "~" H 3900 2600 50  0001 C CNN
	1    3900 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 2250 3900 2450
Connection ~ 3900 2250
$Comp
L Device:Ferrite_Bead FB3
U 1 1 621730F8
P 1350 2850
F 0 "FB3" V 1076 2850 50  0000 C CNN
F 1 "330R" V 1167 2850 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1280 2850 50  0001 C CNN
F 3 "~" H 1350 2850 50  0001 C CNN
	1    1350 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 1450 2050 1450
Wire Wire Line
	3450 2250 3900 2250
$Comp
L Jumper:Jumper_3_Open JP1
U 1 1 6217D3D7
P 2950 1800
F 0 "JP1" V 2904 1887 50  0000 L CNN
F 1 "Jumper_3_Open" V 2995 1887 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 2950 1800 50  0001 C CNN
F 3 "~" H 2950 1800 50  0001 C CNN
	1    2950 1800
	0    -1   1    0   
$EndComp
Wire Wire Line
	2950 2150 2950 2050
Wire Wire Line
	2050 2150 2950 2150
Wire Wire Line
	2950 1550 2950 1350
Wire Wire Line
	2950 1350 2050 1350
Wire Wire Line
	3450 2250 3450 1800
Wire Wire Line
	3450 1800 3100 1800
$Comp
L Regulator_Linear:LM7805_TO220 U4
U 1 1 62181B17
P 4450 4000
F 0 "U4" H 4450 4242 50  0000 C CNN
F 1 "TSR_0.5-2450" H 4450 4151 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_RECOM_R-78E-0.5_THT" H 4450 4225 50  0001 C CIN
F 3 "https://www.onsemi.cn/PowerSolutions/document/MC7800-D.PDF" H 4450 3950 50  0001 C CNN
	1    4450 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 1450 2150 3100
Wire Wire Line
	3900 2750 3900 3100
Wire Wire Line
	1750 2750 1750 3100
Connection ~ 1750 3100
Wire Wire Line
	1750 3100 2150 3100
Connection ~ 2150 3100
Wire Wire Line
	2150 3100 3900 3100
Wire Wire Line
	1650 2750 1650 2850
Wire Wire Line
	1650 2850 1500 2850
Wire Wire Line
	1050 3100 1050 2850
Wire Wire Line
	1050 2850 1200 2850
Wire Wire Line
	1050 3100 1750 3100
Wire Wire Line
	3900 2250 4250 2250
$Comp
L Transistor_FET:IRLML0030 Q3
U 1 1 6218E48F
P 4250 3000
F 0 "Q3" V 4499 3000 50  0000 C CNN
F 1 "IRLML6344" V 4590 3000 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 4450 2925 50  0001 L CIN
F 3 "https://www.infineon.com/dgdl/irlml0030pbf.pdf?fileId=5546d462533600a401535664773825df" H 4250 3000 50  0001 L CNN
	1    4250 3000
	0    -1   1    0   
$EndComp
Wire Wire Line
	4050 3100 3900 3100
Connection ~ 3900 3100
Wire Wire Line
	4250 2800 4250 2250
Connection ~ 4250 2250
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 6219173C
P 1250 4900
AR Path="/6219173C" Ref="J?"  Part="1" 
AR Path="/6216DC24/6219173C" Ref="J9"  Part="1" 
F 0 "J9" H 1330 4892 50  0000 L CNN
F 1 "12VSB" H 1330 4801 50  0000 L CNN
F 2 "TerminalBlock_MetzConnect:TerminalBlock_MetzConnect_Type205_RT04502UBLC_1x02_P5.00mm_45Degree" H 1250 4900 50  0001 C CNN
F 3 "~" H 1250 4900 50  0001 C CNN
	1    1250 4900
	-1   0    0    1   
$EndComp
Wire Wire Line
	1450 4800 1650 4800
Wire Wire Line
	1650 4800 1650 4000
Wire Wire Line
	1650 4000 2250 4000
$Comp
L Device:D_TVS D?
U 1 1 62192F2E
P 2250 4400
AR Path="/62192F2E" Ref="D?"  Part="1" 
AR Path="/6216DC24/62192F2E" Ref="D10"  Part="1" 
F 0 "D10" V 2204 4480 50  0000 L CNN
F 1 "P6SMB_24CA" V 2295 4480 50  0000 L CNN
F 2 "Diode_SMD:D_SMB_Handsoldering" H 2250 4400 50  0001 C CNN
F 3 "~" H 2250 4400 50  0001 C CNN
	1    2250 4400
	0    1    1    0   
$EndComp
$Comp
L Transistor_FET:IRLML0030 Q4
U 1 1 621933CC
P 3000 4800
F 0 "Q4" V 3249 4800 50  0000 C CNN
F 1 "IRLML0040" V 3340 4800 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 3200 4725 50  0001 L CIN
F 3 "https://www.infineon.com/dgdl/irlml0030pbf.pdf?fileId=5546d462533600a401535664773825df" H 3000 4800 50  0001 L CNN
	1    3000 4800
	0    -1   1    0   
$EndComp
Wire Wire Line
	2250 4550 2250 4900
Wire Wire Line
	2250 4250 2250 4000
Wire Wire Line
	3000 4000 3000 4100
Connection ~ 2250 4000
Connection ~ 2250 4900
Wire Wire Line
	2250 4900 2800 4900
Wire Wire Line
	2250 4000 3000 4000
Connection ~ 3000 4000
Wire Wire Line
	3200 4900 3350 4900
Wire Wire Line
	4450 4900 4450 4300
$Comp
L Device:R R14
U 1 1 6219E72B
P 3000 4250
F 0 "R14" H 3070 4296 50  0000 L CNN
F 1 "330k" H 3070 4205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2930 4250 50  0001 C CNN
F 3 "~" H 3000 4250 50  0001 C CNN
	1    3000 4250
	1    0    0    -1  
$EndComp
$Comp
L Diode:BAT54W D11
U 1 1 621A2A93
P 3350 4700
F 0 "D11" V 3304 4780 50  0000 L CNN
F 1 "BZX84 10V" V 3395 4780 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 3350 4525 50  0001 C CNN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/bzx84c2v4.pdf" H 3350 4700 50  0001 C CNN
	1    3350 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 4850 3350 4900
Connection ~ 3350 4900
Wire Wire Line
	3350 4900 3950 4900
Wire Wire Line
	3000 4400 3000 4500
Wire Wire Line
	3350 4550 3350 4500
Wire Wire Line
	3350 4500 3000 4500
Connection ~ 3000 4500
Wire Wire Line
	3000 4500 3000 4600
$Comp
L Jumper:Jumper_3_Open JP2
U 1 1 621AA0D2
P 5750 3500
F 0 "JP2" V 5704 3587 50  0000 L CNN
F 1 "Jumper_3_Open" V 5795 3587 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5750 3500 50  0001 C CNN
F 3 "~" H 5750 3500 50  0001 C CNN
	1    5750 3500
	0    -1   1    0   
$EndComp
Wire Wire Line
	4750 4000 5750 4000
Wire Wire Line
	5750 4000 5750 3750
Wire Wire Line
	5750 2250 5750 3250
Wire Wire Line
	4250 2250 5750 2250
Wire Wire Line
	4450 3100 4950 3100
Wire Wire Line
	4950 3100 4950 4900
Wire Wire Line
	4450 4900 4950 4900
Connection ~ 4950 4900
Wire Wire Line
	4950 4900 4950 6350
Wire Wire Line
	1450 4900 2250 4900
$Comp
L Converter_DCDC:ITX0505SA-R PS1
U 1 1 621D090D
P 4450 6250
F 0 "PS1" H 4450 6631 50  0000 C CNN
F 1 "ITX0505SA-R" H 4450 6540 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_XP_POWER-ITxxxxxS_THT" H 3400 6000 50  0001 L CNN
F 3 "https://www.xppower.com/pdfs/SF_ITX.pdf" H 5500 5950 50  0001 L CNN
	1    4450 6250
	1    0    0    -1  
$EndComp
Text HLabel 1400 6250 0    50   Input ~ 0
DC_DC_~Enable~
Text HLabel 1400 5550 0    50   Input ~ 0
DC_DC_+IN
Text HLabel 1400 7100 0    50   Input ~ 0
DC_DC_-IN
$Comp
L Jumper:Jumper_2_Open JP3
U 1 1 621DF076
P 5600 6150
F 0 "JP3" H 5600 6385 50  0000 C CNN
F 1 "Jumper_2_Open" H 5600 6294 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5600 6150 50  0001 C CNN
F 3 "~" H 5600 6150 50  0001 C CNN
	1    5600 6150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5800 6150 6200 6150
Wire Wire Line
	6200 3500 5900 3500
Wire Wire Line
	4950 4900 6550 4900
Wire Wire Line
	8700 4100 7500 4100
Text HLabel 8700 4100 2    50   Input ~ 0
Supply_5V
Text HLabel 8700 4900 2    50   Input ~ 0
Supply_GND
Text HLabel 8700 4300 2    50   Input ~ 0
Supply_3V3
$Comp
L Regulator_Linear:MIC5365-3.3YD5 U5
U 1 1 621E889F
P 8000 4400
F 0 "U5" H 8000 4767 50  0000 C CNN
F 1 "MIC5365-3.3YD5" H 8000 4676 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 8000 4750 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/mic5365.pdf" H 7750 4650 50  0001 C CNN
	1    8000 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 4300 8550 4300
Wire Wire Line
	8000 4700 8000 4900
Connection ~ 8000 4900
Wire Wire Line
	8000 4900 8550 4900
Wire Wire Line
	7600 4500 7500 4500
Wire Wire Line
	7500 4500 7500 4300
Connection ~ 7500 4100
Wire Wire Line
	7600 4300 7500 4300
Connection ~ 7500 4300
Wire Wire Line
	7500 4300 7500 4100
$Comp
L Device:C C17
U 1 1 621EF9D9
P 8550 4600
F 0 "C17" H 8665 4646 50  0000 L CNN
F 1 "1u" H 8665 4555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 8588 4450 50  0001 C CNN
F 3 "~" H 8550 4600 50  0001 C CNN
	1    8550 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 4450 8550 4300
Connection ~ 8550 4300
Wire Wire Line
	8550 4300 8700 4300
Wire Wire Line
	8550 4750 8550 4900
Connection ~ 8550 4900
Wire Wire Line
	8550 4900 8700 4900
$Comp
L Device:C C16
U 1 1 621F25FD
P 7250 4600
F 0 "C16" H 7365 4646 50  0000 L CNN
F 1 "1u" H 7365 4555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 7288 4450 50  0001 C CNN
F 3 "~" H 7250 4600 50  0001 C CNN
	1    7250 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 4900 7250 4750
Connection ~ 7250 4900
Wire Wire Line
	7250 4900 8000 4900
Wire Wire Line
	7250 4450 7250 4300
Wire Wire Line
	7250 4300 7500 4300
$Comp
L Device:Ferrite_Bead FB4
U 1 1 621F7F26
P 7100 4100
F 0 "FB4" V 7374 4100 50  0000 C CNN
F 1 "330R" V 7283 4100 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7030 4100 50  0001 C CNN
F 3 "~" H 7100 4100 50  0001 C CNN
	1    7100 4100
	0    -1   -1   0   
$EndComp
$Comp
L Device:Ferrite_Bead FB5
U 1 1 621F939C
P 3150 5550
F 0 "FB5" V 2876 5550 50  0000 C CNN
F 1 "330R" V 2967 5550 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3080 5550 50  0001 C CNN
F 3 "~" H 3150 5550 50  0001 C CNN
	1    3150 5550
	0    1    1    0   
$EndComp
$Comp
L Device:C C18
U 1 1 62203B40
P 3500 5800
F 0 "C18" H 3385 5754 50  0000 R CNN
F 1 "1u" H 3385 5845 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3538 5650 50  0001 C CNN
F 3 "~" H 3500 5800 50  0001 C CNN
	1    3500 5800
	-1   0    0    1   
$EndComp
Wire Wire Line
	3500 5650 3500 5550
Wire Wire Line
	3500 5950 3500 7100
Wire Wire Line
	5400 6150 4850 6150
Wire Wire Line
	3900 7100 3900 6350
Wire Wire Line
	3900 5550 3900 6150
$Comp
L Logic_LevelTranslator:SN74LV1T34DBV U6
U 1 1 622874F7
P 2800 6250
F 0 "U6" H 3144 6296 50  0000 L CNN
F 1 "M74VHC 1GT 50 DTT1G" H 3144 6205 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 3450 6000 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/sn74lv1t34.pdf" H 2400 6050 50  0001 C CNN
	1    2800 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 5950 2800 5700
Wire Wire Line
	2800 6550 2800 6650
Connection ~ 3500 5550
Wire Wire Line
	3500 5550 3900 5550
Connection ~ 3500 7100
Wire Wire Line
	3500 7100 3900 7100
Connection ~ 2800 5550
Connection ~ 2800 7100
Wire Wire Line
	2800 5550 3000 5550
Wire Wire Line
	2800 7100 3500 7100
Wire Wire Line
	4850 6350 4950 6350
Wire Wire Line
	3900 6350 4050 6350
Wire Wire Line
	4050 6150 3900 6150
Wire Wire Line
	3100 6250 4050 6250
Wire Wire Line
	1400 6250 2500 6250
Wire Wire Line
	3300 5550 3500 5550
$Comp
L Device:C C19
U 1 1 622DC088
P 2250 5950
F 0 "C19" H 2135 5904 50  0000 R CNN
F 1 "100n" H 2135 5995 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 2288 5800 50  0001 C CNN
F 3 "~" H 2250 5950 50  0001 C CNN
	1    2250 5950
	-1   0    0    1   
$EndComp
Wire Wire Line
	1400 5550 2800 5550
Wire Wire Line
	1400 7100 2800 7100
Wire Wire Line
	2250 5800 2250 5700
Wire Wire Line
	2250 5700 2800 5700
Connection ~ 2800 5700
Wire Wire Line
	2800 5700 2800 5550
Wire Wire Line
	2250 6100 2250 6650
Wire Wire Line
	2250 6650 2800 6650
Connection ~ 2800 6650
Wire Wire Line
	2800 6650 2800 7100
Text Notes 3100 6550 0    50   ~ 0
Level\nshifter
Text HLabel 8700 3750 2    50   Input ~ 0
Supply_5VP
Wire Wire Line
	7250 4100 7500 4100
Wire Wire Line
	6200 3500 6200 3750
Wire Wire Line
	8700 3750 6850 3750
Connection ~ 6200 3750
Wire Wire Line
	6200 3750 6200 6150
Wire Wire Line
	6950 4100 6850 4100
Wire Wire Line
	6850 4100 6850 3750
Connection ~ 6850 3750
Wire Wire Line
	6850 3750 6550 3750
$Comp
L Device:C C15
U 1 1 6233F118
P 6550 4450
F 0 "C15" H 6665 4496 50  0000 L CNN
F 1 "1u" H 6665 4405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 6588 4300 50  0001 C CNN
F 3 "~" H 6550 4450 50  0001 C CNN
	1    6550 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 4300 6550 3750
Connection ~ 6550 3750
Wire Wire Line
	6550 3750 6200 3750
Wire Wire Line
	6550 4600 6550 4900
Connection ~ 6550 4900
Wire Wire Line
	6550 4900 7250 4900
$Comp
L Device:C C20
U 1 1 6206734A
P 3950 4400
F 0 "C20" H 3835 4354 50  0000 R CNN
F 1 "1u" H 3835 4445 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3988 4250 50  0001 C CNN
F 3 "~" H 3950 4400 50  0001 C CNN
	1    3950 4400
	-1   0    0    1   
$EndComp
Wire Wire Line
	3950 4550 3950 4900
Wire Wire Line
	3950 4900 4450 4900
Connection ~ 3950 4900
Connection ~ 4450 4900
Wire Wire Line
	3000 4000 3350 4000
Wire Wire Line
	3950 4250 3950 4000
Connection ~ 3950 4000
Wire Wire Line
	3950 4000 4150 4000
$Comp
L Device:Ferrite_Bead FB6
U 1 1 620757AB
P 3500 4000
F 0 "FB6" V 3226 4000 50  0000 C CNN
F 1 "330R" V 3317 4000 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3430 4000 50  0001 C CNN
F 3 "~" H 3500 4000 50  0001 C CNN
	1    3500 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	3650 4000 3950 4000
Wire Notes Line
	4450 7350 4450 5200
Wire Notes Line
	4450 5200 750  5200
$Comp
L Device:R R?
U 1 1 61CB4E12
P 4300 7100
AR Path="/61CB4E12" Ref="R?"  Part="1" 
AR Path="/6216DC24/61CB4E12" Ref="R18"  Part="1" 
F 0 "R18" V 4093 7100 50  0000 C CNN
F 1 "1M" V 4184 7100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 4230 7100 50  0001 C CNN
F 3 "~" H 4300 7100 50  0001 C CNN
	1    4300 7100
	0    1    1    0   
$EndComp
Wire Wire Line
	4450 7100 4950 7100
Wire Wire Line
	4950 7100 4950 6350
Connection ~ 4950 6350
Wire Wire Line
	4150 7100 3900 7100
Connection ~ 3900 7100
$EndSCHEMATC
