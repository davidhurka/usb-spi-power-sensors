EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transistor_FET:2N7002 Q?
U 1 1 6258F93C
P 2350 5250
AR Path="/6258F93C" Ref="Q?"  Part="1" 
AR Path="/62547F3A/6258F93C" Ref="Q1"  Part="1" 
F 0 "Q1" H 2555 5296 50  0000 L CNN
F 1 "FDV 303N" H 2555 5205 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 2550 5175 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/NDS7002A-D.PDF" H 2350 5250 50  0001 L CNN
	1    2350 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 5250 1750 5250
$Comp
L Device:R R?
U 1 1 6258F94C
P 1900 5250
AR Path="/6258F94C" Ref="R?"  Part="1" 
AR Path="/62547F3A/6258F94C" Ref="R3"  Part="1" 
F 0 "R3" V 1693 5250 50  0000 C CNN
F 1 "10k" V 1784 5250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 1830 5250 50  0001 C CNN
F 3 "~" H 1900 5250 50  0001 C CNN
	1    1900 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	2050 5250 2150 5250
$Comp
L Relay:SILxx-1Axx-71x K?
U 1 1 6258F953
P 2650 4050
AR Path="/6258F953" Ref="K?"  Part="1" 
AR Path="/62547F3A/6258F953" Ref="K1"  Part="1" 
F 0 "K1" H 2220 4004 50  0000 R CNN
F 1 "SILxx-1Axx-71x" H 2220 4095 50  0000 R CNN
F 2 "Relay_THT:Relay_SPST_StandexMeder_SIL_Form1A" H 3000 4000 50  0001 L CNN
F 3 "https://standexelectronics.com/wp-content/uploads/datasheet_reed_relay_SIL.pdf" H 2650 4050 50  0001 C CNN
	1    2650 4050
	1    0    0    1   
$EndComp
Wire Wire Line
	2450 3450 2000 3450
Wire Wire Line
	2000 4550 2450 4550
Connection ~ 2450 4550
$Comp
L Diode:BAS21 D?
U 1 1 6258F961
P 2000 3900
AR Path="/6258F961" Ref="D?"  Part="1" 
AR Path="/62547F3A/6258F961" Ref="D1"  Part="1" 
F 0 "D1" V 1950 3650 50  0000 L CNN
F 1 "BAS21" V 2050 3600 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 2000 3725 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/Ds12004.pdf" H 2000 3900 50  0001 C CNN
	1    2000 3900
	0    1    1    0   
$EndComp
$Comp
L Transistor_FET:2N7002 Q?
U 1 1 6258F968
P 2350 2550
AR Path="/6258F968" Ref="Q?"  Part="1" 
AR Path="/62547F3A/6258F968" Ref="Q2"  Part="1" 
F 0 "Q2" H 2555 2596 50  0000 L CNN
F 1 "FDV 303N" H 2555 2505 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 2550 2475 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/NDS7002A-D.PDF" H 2350 2550 50  0001 L CNN
	1    2350 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 2850 2450 2750
$Comp
L Relay:SILxx-1Axx-71x K?
U 1 1 6258F975
P 2650 1350
AR Path="/6258F975" Ref="K?"  Part="1" 
AR Path="/62547F3A/6258F975" Ref="K2"  Part="1" 
F 0 "K2" H 2220 1304 50  0000 R CNN
F 1 "SILxx-1Axx-71x" H 2220 1395 50  0000 R CNN
F 2 "Relay_THT:Relay_SPST_StandexMeder_SIL_Form1A" H 3000 1300 50  0001 L CNN
F 3 "https://standexelectronics.com/wp-content/uploads/datasheet_reed_relay_SIL.pdf" H 2650 1350 50  0001 C CNN
	1    2650 1350
	1    0    0    1   
$EndComp
Wire Wire Line
	2450 850  2000 850 
Wire Wire Line
	2000 1850 2450 1850
Connection ~ 2450 1850
$Comp
L Device:R R?
U 1 1 6258F995
P 2450 2100
AR Path="/6258F995" Ref="R?"  Part="1" 
AR Path="/62547F3A/6258F995" Ref="R19"  Part="1" 
F 0 "R19" V 2243 2100 50  0000 C CNN
F 1 "0R" V 2334 2100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2380 2100 50  0001 C CNN
F 3 "~" H 2450 2100 50  0001 C CNN
	1    2450 2100
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 625E4D7B
P 1900 2550
AR Path="/625E4D7B" Ref="R?"  Part="1" 
AR Path="/62547F3A/625E4D7B" Ref="R6"  Part="1" 
F 0 "R6" V 1693 2550 50  0000 C CNN
F 1 "10k" V 1784 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 1830 2550 50  0001 C CNN
F 3 "~" H 1900 2550 50  0001 C CNN
	1    1900 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	2050 2550 2150 2550
$Comp
L Diode:BAS21 D?
U 1 1 625E4D83
P 2000 1200
AR Path="/625E4D83" Ref="D?"  Part="1" 
AR Path="/62547F3A/625E4D83" Ref="D3"  Part="1" 
F 0 "D3" V 1950 950 50  0000 L CNN
F 1 "BAS21" V 2050 900 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 2000 1025 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/Ds12004.pdf" H 2000 1200 50  0001 C CNN
	1    2000 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	1250 2550 1750 2550
Wire Wire Line
	2450 4550 2450 4650
$Comp
L Device:R R?
U 1 1 6261CB21
P 2450 4800
AR Path="/6261CB21" Ref="R?"  Part="1" 
AR Path="/62547F3A/6261CB21" Ref="R20"  Part="1" 
F 0 "R20" V 2243 4800 50  0000 C CNN
F 1 "0R" V 2334 4800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2380 4800 50  0001 C CNN
F 3 "~" H 2450 4800 50  0001 C CNN
	1    2450 4800
	-1   0    0    1   
$EndComp
Wire Wire Line
	2450 4950 2450 5050
Wire Wire Line
	2450 1850 2450 1950
Wire Wire Line
	2450 2250 2450 2350
Wire Wire Line
	3300 3550 3300 3200
Text HLabel 1250 2550 0    50   Input ~ 0
Relais1
Text HLabel 1250 5250 0    50   Input ~ 0
Relais2
Wire Wire Line
	2000 3450 1650 3450
Wire Wire Line
	1650 850  2000 850 
Connection ~ 2000 3450
Connection ~ 2000 850 
Wire Wire Line
	1250 850  1650 850 
Wire Wire Line
	1250 5550 1550 5550
Wire Wire Line
	2450 5450 2450 5550
Wire Wire Line
	2450 2850 1550 2850
Wire Wire Line
	1550 5550 2450 5550
Text HLabel 1250 850  0    50   Input ~ 0
Relais_VCC
Text HLabel 1250 5550 0    50   Input ~ 0
Relais_GND
Wire Wire Line
	2850 3550 3300 3550
Connection ~ 1550 5550
Connection ~ 1650 850 
Wire Wire Line
	2850 4450 3400 4450
Connection ~ 3600 2900
Wire Wire Line
	3400 2900 3600 2900
Wire Wire Line
	3400 3300 3600 3300
Wire Wire Line
	3600 3300 4100 3300
Wire Wire Line
	3300 3200 3600 3200
Wire Wire Line
	3600 3200 4100 3200
Wire Wire Line
	3600 3000 4100 3000
Connection ~ 3600 3300
Connection ~ 3600 3200
Connection ~ 3600 3000
Wire Wire Line
	3600 2900 4100 2900
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 625C5B0B
P 3800 3100
AR Path="/625C5B0B" Ref="J?"  Part="1" 
AR Path="/62547F3A/625C5B0B" Ref="J1"  Part="1" 
F 0 "J1" H 3850 3517 50  0000 C CNN
F 1 "Relais" H 3850 3426 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Horizontal" H 3800 3100 50  0001 C CNN
F 3 "~" H 3800 3100 50  0001 C CNN
	1    3800 3100
	1    0    0    1   
$EndComp
$Comp
L Relay:G5V-1 K3
U 1 1 62840A52
P 4050 1350
F 0 "K3" H 4480 1396 50  0000 L CNN
F 1 "G5V-1" H 4480 1305 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_Omron_G5V-1" H 5180 1320 50  0001 C CNN
F 3 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-g5v_1.pdf" H 4050 1350 50  0001 C CNN
	1    4050 1350
	1    0    0    -1  
$EndComp
$Comp
L Relay:G5V-1 K4
U 1 1 62841147
P 4050 4050
F 0 "K4" H 4480 4096 50  0000 L CNN
F 1 "G5V-1" H 4480 4005 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_Omron_G5V-1" H 5180 4020 50  0001 C CNN
F 3 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-g5v_1.pdf" H 4050 4050 50  0001 C CNN
	1    4050 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 1650 2850 1750
Wire Wire Line
	2450 1650 2450 1850
Wire Wire Line
	2000 1350 2000 1850
Wire Wire Line
	2450 850  2450 1050
Wire Wire Line
	2000 850  2000 1050
Wire Wire Line
	1650 850  1650 3450
Wire Wire Line
	2450 4350 2450 4550
Wire Wire Line
	2850 4350 2850 4450
Wire Wire Line
	3400 3300 3400 4450
Wire Wire Line
	2850 3550 2850 3750
Wire Wire Line
	2450 3450 2450 3650
Wire Wire Line
	2000 3450 2000 3750
Wire Wire Line
	2000 4050 2000 4550
Wire Wire Line
	1550 2850 1550 5550
Wire Wire Line
	3850 1050 3850 850 
Wire Wire Line
	3850 850  2450 850 
Connection ~ 2450 850 
Wire Wire Line
	3850 1650 3850 1850
Wire Wire Line
	3850 1850 2450 1850
Wire Wire Line
	4350 950  4350 1050
Wire Wire Line
	4250 1650 4250 1750
Wire Wire Line
	4250 1750 3400 1750
Wire Wire Line
	3300 3000 3600 3000
Wire Wire Line
	3300 3550 4350 3550
Wire Wire Line
	4350 3550 4350 3750
Connection ~ 3300 3550
Wire Wire Line
	4250 4350 4250 4450
Wire Wire Line
	4250 4450 3400 4450
Connection ~ 3400 4450
Wire Wire Line
	2450 3650 3850 3650
Wire Wire Line
	3850 3650 3850 3750
Connection ~ 2450 3650
Wire Wire Line
	2450 3650 2450 3750
Wire Wire Line
	3850 4350 3850 4550
Wire Wire Line
	3850 4550 2450 4550
Connection ~ 3400 1750
Wire Wire Line
	3400 1750 3400 2900
Wire Wire Line
	2850 1750 3300 1750
Wire Wire Line
	3300 950  3300 1750
Wire Wire Line
	3300 950  4350 950 
Wire Wire Line
	3400 1750 3400 750 
Wire Wire Line
	3400 750  2850 750 
Wire Wire Line
	2850 750  2850 1050
Connection ~ 3300 1750
Wire Wire Line
	3300 1750 3300 3000
$Comp
L Device:LED D?
U 1 1 62A32270
P 1950 7050
AR Path="/62A32270" Ref="D?"  Part="1" 
AR Path="/62547F3A/62A32270" Ref="D8"  Part="1" 
F 0 "D8" H 1943 7267 50  0000 C CNN
F 1 "LED" H 1943 7176 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1950 7050 50  0001 C CNN
F 3 "~" H 1950 7050 50  0001 C CNN
	1    1950 7050
	0    1    -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 62A32276
P 1650 7050
AR Path="/62A32276" Ref="D?"  Part="1" 
AR Path="/62547F3A/62A32276" Ref="D7"  Part="1" 
F 0 "D7" H 1643 7267 50  0000 C CNN
F 1 "LED" H 1643 7176 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1650 7050 50  0001 C CNN
F 3 "~" H 1650 7050 50  0001 C CNN
	1    1650 7050
	0    1    -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 62A3227C
P 1350 7050
AR Path="/62A3227C" Ref="D?"  Part="1" 
AR Path="/62547F3A/62A3227C" Ref="D6"  Part="1" 
F 0 "D6" H 1343 7267 50  0000 C CNN
F 1 "LED" H 1343 7176 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1350 7050 50  0001 C CNN
F 3 "~" H 1350 7050 50  0001 C CNN
	1    1350 7050
	0    1    -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 62A32282
P 1050 7050
AR Path="/62A32282" Ref="D?"  Part="1" 
AR Path="/62547F3A/62A32282" Ref="D5"  Part="1" 
F 0 "D5" H 1043 7267 50  0000 C CNN
F 1 "LED" H 1043 7176 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1050 7050 50  0001 C CNN
F 3 "~" H 1050 7050 50  0001 C CNN
	1    1050 7050
	0    1    -1   0   
$EndComp
Wire Wire Line
	1350 7200 1350 7300
Wire Wire Line
	1650 7300 1650 7200
Wire Wire Line
	1650 7300 1950 7300
Wire Wire Line
	1950 7300 1950 7200
Connection ~ 1650 7300
Wire Wire Line
	1350 7300 1050 7300
Connection ~ 1350 7300
$Comp
L power:GNDD #PWR?
U 1 1 62A3228F
P 1950 7400
AR Path="/62A3228F" Ref="#PWR?"  Part="1" 
AR Path="/62547F3A/62A3228F" Ref="#PWR0143"  Part="1" 
F 0 "#PWR0143" H 1950 7150 50  0001 C CNN
F 1 "GNDD" H 1954 7245 50  0000 C CNN
F 2 "" H 1950 7400 50  0001 C CNN
F 3 "" H 1950 7400 50  0001 C CNN
	1    1950 7400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 62A32295
P 1950 6650
AR Path="/62A32295" Ref="R?"  Part="1" 
AR Path="/62547F3A/62A32295" Ref="R13"  Part="1" 
F 0 "R13" V 1743 6650 50  0000 C CNN
F 1 "1k" V 1834 6650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 1880 6650 50  0001 C CNN
F 3 "~" H 1950 6650 50  0001 C CNN
	1    1950 6650
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 62A3229B
P 1650 6650
AR Path="/62A3229B" Ref="R?"  Part="1" 
AR Path="/62547F3A/62A3229B" Ref="R12"  Part="1" 
F 0 "R12" V 1443 6650 50  0000 C CNN
F 1 "1k" V 1534 6650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 1580 6650 50  0001 C CNN
F 3 "~" H 1650 6650 50  0001 C CNN
	1    1650 6650
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 62A322A1
P 1050 6650
AR Path="/62A322A1" Ref="R?"  Part="1" 
AR Path="/62547F3A/62A322A1" Ref="R10"  Part="1" 
F 0 "R10" V 843 6650 50  0000 C CNN
F 1 "1k" V 934 6650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 980 6650 50  0001 C CNN
F 3 "~" H 1050 6650 50  0001 C CNN
	1    1050 6650
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 62A322A7
P 1350 6650
AR Path="/62A322A7" Ref="R?"  Part="1" 
AR Path="/62547F3A/62A322A7" Ref="R11"  Part="1" 
F 0 "R11" V 1143 6650 50  0000 C CNN
F 1 "1k" V 1234 6650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 1280 6650 50  0001 C CNN
F 3 "~" H 1350 6650 50  0001 C CNN
	1    1350 6650
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 7300 1350 7300
Wire Wire Line
	1050 7300 1050 7200
Wire Wire Line
	1050 6900 1050 6800
Wire Wire Line
	1350 6800 1350 6900
Wire Wire Line
	1650 6900 1650 6800
Wire Wire Line
	1950 6800 1950 6900
Wire Wire Line
	1950 7400 1950 7300
Connection ~ 1950 7300
Wire Wire Line
	1050 6500 1050 6400
Wire Wire Line
	1350 6400 1350 6500
Wire Wire Line
	1650 6500 1650 6400
Wire Wire Line
	1950 6400 1950 6500
Text HLabel 1050 6400 1    50   Input ~ 0
LED1
Text HLabel 1350 6400 1    50   Input ~ 0
LED2
Text HLabel 1650 6400 1    50   Input ~ 0
LED3
Text HLabel 1950 6400 1    50   Input ~ 0
LED4
$Comp
L Switch:SW_Push SW?
U 1 1 62B581E2
P 3450 6400
AR Path="/62B581E2" Ref="SW?"  Part="1" 
AR Path="/62547F3A/62B581E2" Ref="SW2"  Part="1" 
F 0 "SW2" H 3450 6685 50  0000 C CNN
F 1 "SW_Push" H 3450 6594 50  0000 C CNN
F 2 "parts:Schurter_LSG_1301.931x_SMD_Button" H 3450 6600 50  0001 C CNN
F 3 "~" H 3450 6600 50  0001 C CNN
	1    3450 6400
	-1   0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW?
U 1 1 62B581E8
P 3450 6800
AR Path="/62B581E8" Ref="SW?"  Part="1" 
AR Path="/62547F3A/62B581E8" Ref="SW3"  Part="1" 
F 0 "SW3" H 3450 7085 50  0000 C CNN
F 1 "SW_Push" H 3450 6994 50  0000 C CNN
F 2 "parts:Schurter_LSG_1301.931x_SMD_Button" H 3450 7000 50  0001 C CNN
F 3 "~" H 3450 7000 50  0001 C CNN
	1    3450 6800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3650 6400 3750 6400
Wire Wire Line
	3750 6400 3750 6800
Wire Wire Line
	3650 6800 3750 6800
Connection ~ 3750 6800
Wire Wire Line
	3750 6800 3750 6900
$Comp
L power:GNDD #PWR?
U 1 1 62B581F3
P 3750 6900
AR Path="/62B581F3" Ref="#PWR?"  Part="1" 
AR Path="/62547F3A/62B581F3" Ref="#PWR0151"  Part="1" 
F 0 "#PWR0151" H 3750 6650 50  0001 C CNN
F 1 "GNDD" H 3754 6745 50  0000 C CNN
F 2 "" H 3750 6900 50  0001 C CNN
F 3 "" H 3750 6900 50  0001 C CNN
	1    3750 6900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3250 6400 2750 6400
Wire Wire Line
	3250 6800 2750 6800
Wire Notes Line
	3900 7200 3900 6150
Wire Notes Line
	3900 6150 2800 6150
Wire Notes Line
	2800 6150 2800 7200
Wire Notes Line
	2800 7200 3900 7200
Text Notes 3600 7150 2    50   ~ 0
Optional
Text HLabel 2750 6800 0    50   Input ~ 0
Button2
Text HLabel 2750 6400 0    50   Input ~ 0
Button1
$EndSCHEMATC
