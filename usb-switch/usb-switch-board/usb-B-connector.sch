EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 21
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB_B J?
U 1 1 61DA1369
P 4500 3650
AR Path="/61DA1369" Ref="J?"  Part="1" 
AR Path="/61AE8AE6/61DA1369" Ref="J?"  Part="1" 
AR Path="/61B04C8D/61DA1369" Ref="J?"  Part="1" 
AR Path="/61B04C8D/61D9E2BC/61DA1369" Ref="J4"  Part="1" 
AR Path="/61B04C8D/61DC40E2/61DA1369" Ref="J6"  Part="1" 
AR Path="/61AE8AE6/62349367/61DA1369" Ref="J2"  Part="1" 
F 0 "J2" H 4557 4117 50  0000 C CNN
F 1 "USB_B" H 4557 4026 50  0000 C CNN
F 2 "Connector_USB:USB_B_OST_USB-B1HSxx_Horizontal" H 4650 3600 50  0001 C CNN
F 3 " ~" H 4650 3600 50  0001 C CNN
	1    4500 3650
	1    0    0    -1  
$EndComp
$Comp
L Power_Protection:USBLC6-4SC6 U?
U 1 1 61DA13A3
P 7300 3000
AR Path="/61AE8AE6/61DA13A3" Ref="U?"  Part="1" 
AR Path="/61B04C8D/61DA13A3" Ref="U?"  Part="1" 
AR Path="/61B04C8D/61D9E2BC/61DA13A3" Ref="U10"  Part="1" 
AR Path="/61B04C8D/61DC40E2/61DA13A3" Ref="U11"  Part="1" 
AR Path="/61AE8AE6/62349367/61DA13A3" Ref="U4"  Part="1" 
F 0 "U4" H 7400 3500 50  0000 C CNN
F 1 "USBLC6-4SC6" H 7600 3400 50  0000 C CNN
F 2 "parts:SOT-23-6_USB_ESD" H 7300 2500 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/usblc6-4.pdf" H 7500 3350 50  0001 C CNN
	1    7300 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 3100 6800 3100
Text HLabel 8150 2250 2    50   Input ~ 0
VBUS
Wire Wire Line
	7800 2900 7700 2900
Wire Wire Line
	6800 3100 6800 3650
Text HLabel 8150 3650 2    50   Input ~ 0
D+
Text HLabel 8150 3750 2    50   Input ~ 0
D-
Wire Wire Line
	8150 3750 7800 3750
Connection ~ 7800 3750
Wire Wire Line
	7800 2900 7800 3750
Wire Wire Line
	4800 3650 6800 3650
Connection ~ 6800 3650
Wire Wire Line
	6800 3650 8150 3650
Wire Wire Line
	4800 3750 7800 3750
$Sheet
S 5050 2150 1000 550 
U 622A5C9C
F0 "USB-B_Connector_1_Power" 50
F1 "usb-connector-power.sch" 50
F2 "Connector_VBUS" I L 5050 2600 50 
F3 "ESD_VBUS" I R 6050 2400 50 
F4 "VBUS" I R 6050 2250 50 
F5 "GND" I R 6050 2600 50 
$EndSheet
$Sheet
S 4850 4150 1200 450 
U 622ACD64
F0 "USB-B_Connector_1_GND" 50
F1 "usb-connector-ground.sch" 50
F2 "Connector_GND" I L 4850 4300 50 
F3 "Connector_Shield" I L 4850 4450 50 
F4 "ESD_GND" I R 6050 4300 50 
F5 "GND" I R 6050 4450 50 
$EndSheet
Wire Wire Line
	4850 4300 4500 4300
Wire Wire Line
	4500 4300 4500 4050
Wire Wire Line
	4400 4050 4400 4450
Wire Wire Line
	4400 4450 4850 4450
Wire Wire Line
	7300 3400 7300 4300
Wire Wire Line
	7300 2400 7300 2600
Wire Wire Line
	5050 2600 4950 2600
Wire Wire Line
	4950 2600 4950 3450
Wire Wire Line
	4950 3450 4800 3450
Wire Wire Line
	6050 4300 7300 4300
Wire Wire Line
	6050 2250 8150 2250
Text HLabel 8150 4450 2    50   Input ~ 0
GND
Wire Wire Line
	8150 4450 6550 4450
Wire Wire Line
	6050 2400 7300 2400
Wire Wire Line
	6050 2600 6550 2600
Wire Wire Line
	6550 2600 6550 4450
Connection ~ 6550 4450
Wire Wire Line
	6550 4450 6050 4450
$EndSCHEMATC
