<!--
SPDX-FileCopyrightText: 2021 David Hurka <david.hurka@mailbox.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# USB Switch

This board can be connected to a GPIO header of a power sensor module, and can enable or disable the connection to e. g. a USB thumb drive.

This is done by disconnecting the USB D+ and D- data lines using an “analog switch” IC.

At least Intenso Rainbow Line USB thumb drives do not need a power cycle on the VBus line, so switching the data lines should be sufficient.
