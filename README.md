<!--
SPDX-FileCopyrightText: 2021 David Hurka <david.hurka@mailbox.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# USB SPI power sensors

This project will hold manufacturing data for modular power sensors which can be connected to internal power lanes of ATX (and similar) computers.

The currently available power sensors have 5 channels each, use an NXP KL13z microcontroller, and communicate via daisy-chained SPI with one master controller.

The master controller provides the USB interface, and provides basic electric insulation.
Electric insulation shall allow to carelessly plug the USB cable into any other Data Aggregator (DAE), e. g. a battery-powered laptop with a long Ethernet cable running to another office.
The master controller additionally provides two relais channels, which can emulate pressing the power button on the System under Test (SuT)

The sensors shall provide 10mV and 1mA resolution at 10kHz, at +/- 18V and +/- 20A.
They provide 8 additional general purpose IO pins, which may be used e. g. to enable a recovery thumb drive in case the SuT gets messed up.

## Purpose

These power sensors are intended to provide a higher resolution alternative to Power Meters (PM) measuring at the wall outlet.
The PM is a component in energy-efficiency measurement setups used by the KDE Eco project.
