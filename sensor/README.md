<!--
SPDX-FileCopyrightText: 2021 David Hurka <david.hurka@mailbox.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Sensor Module

This is a power meter module built around the NXP KL13z microcontroller and TMCS1100 and MAX9919 current sensors.
The module provides 5 channels up to ±20A and ±18V, so it can be used to measure the power consumption of any typical desktop computer component.
The ADC of the KL13z provides 16 Bit resolution at 10 kilohertz (and more) sampling rate.

The module is implemented on a 2-layer PCB with SMD components.
It provides double screw terminals for the power lines, and two 2.54mm IDC headers for communication with the master module and the next sensor module.
A third IDC header provides 8 GPIO pins, which can be used for auxiliary functions.
